package com.example.spring_itadakimasu.repository;

import com.example.spring_itadakimasu.entities.CartItem;
import com.example.spring_itadakimasu.entities.Meal;
import com.example.spring_itadakimasu.entities.MealType;
import com.example.spring_itadakimasu.entities.User;
import com.example.spring_itadakimasu.repositories.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.TestPropertySource;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.core.IsEqual.equalTo;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@TestPropertySource(locations="classpath:test.properties") //Para usar o file test.properties e não o application.properties (usa a BD de testes)
class UserRepositoryTests {
    MealType pizza;
    MealType pasta;
    Meal m1;
    Meal m2;
    CartItem item1;
    CartItem item2;
    @Autowired
    private UserRepository userRep;

    @Autowired
    private TestEntityManager entityManager;

    @BeforeEach
    void setUp() {
        // Save User
        User user = new User();
        user.setEmail("testEmail@gmail.com");
        user.setPassword("testpass");
        user.setFirstName("Tes");
        user.setLastName("Te");
        user.setPhoneNumber("123456789");
        userRep.saveAndFlush(user);

        // Get User CartItems Ready
        pizza = new MealType("pizza");
        pasta = new MealType("pasta");
        m1 = new Meal(pizza,"Bacon Pizza","Large bacon pizza with tomato sauce", 10, 10.0);
        m2 = new Meal(pasta,"Carbonara","Medium carbonara with bechamel", 6, 11.6);
        item1 = new  CartItem(m1,1);
        item2 = new  CartItem(m2,1);
        entityManager.persistAndFlush(pizza);
        entityManager.persistAndFlush(pasta);
        entityManager.persistAndFlush(m1);
        entityManager.persistAndFlush(m2);
        entityManager.persistAndFlush(item1);
        entityManager.persistAndFlush(item2);
    }


    // ########## CREATED USER ##########

    @Test
    void testfindUser() {
        User testUser = userRep.findByEmail("testEmail@gmail.com");
        assertThat(testUser.getEmail(), equalTo("testEmail@gmail.com"));
        assertThat(testUser.getFirstName(), equalTo("Tes"));
        assertThat(testUser.getLastName(), equalTo("Te"));
        assertThat(testUser.getPhoneNumber(), equalTo("123456789"));
    }


    // ########## USER's CART ##########
    @Test
    void whenMealInCart_thenReturnUserCartWithMeal(){
        User testUser = userRep.findByEmail("testEmail@gmail.com");
        testUser.getCart().add(item1);
        entityManager.persistAndFlush(testUser);
        testUser = userRep.findByEmail("testEmail@gmail.com");

        assertThat(testUser.getCart(),hasItem(item1));
        assertThat(testUser.getCart(), hasItem(allOf(
                hasProperty("meal", is(m1)),
                hasProperty("qty", is(1)))));

    }

    @Test
    void whenCartIsEmpty_thenReturnUserCartEmpty(){
        User testUser = userRep.findByEmail("testEmail@gmail.com");
        assertThat(testUser.getCart(),hasSize(0));
    }

    @Test
    void whenCartHas2Meals_thenReturnUserCart2Meals(){
        User testUser = userRep.findByEmail("testEmail@gmail.com");
        testUser.getCart().add(item1);
        testUser.getCart().add(item2);
        entityManager.persistAndFlush(testUser);
        testUser = userRep.findByEmail("testEmail@gmail.com");

        assertThat(testUser.getCart(),hasSize(2));
        assertThat(testUser.getCart(),hasItems(item1,item2));
    }
}

package com.example.spring_itadakimasu.controllers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import io.restassured.RestAssured;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.hamcrest.CoreMatchers.*;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations="classpath:test.properties") //isto serve para usar o ficheiro test.properties e não o application.properties (usa a BD de testes)
class ItadakimasuRestControllerIT {

    @LocalServerPort
    int port;

    // For Endpoints that need authentication -- Start --
    @Autowired
    private WebApplicationContext context;

    private MockMvc mvc;

    @BeforeEach
    public void setup() {
        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }
    // For Endpoints that need authentication -- End --

    @DisplayName("Search Meals with Bacon and they exist")
    @Test
    void mealExists(){
        RestAssured.given().port(port).get("/api/meals?search=bacon")
                .then()
                .statusCode(200)
                .and().body("name[0]",containsString("Bacon With Cheddar"))
                .and().body("name[1]",containsString("Carbonara with bacon"))
                .and().body("name[2]",containsString("Lasagna with BaCoN"));
    }

    @DisplayName("Search Meals with asd and they dont exist")
    @Test
    void mealDoesntExists(){
        RestAssured.given().port(port).get("/api/meals?search=asd")
                .then()
                .statusCode(404);
    }

    @DisplayName("Search All Meals")
    @Test
    void mealAll(){
        RestAssured.given().port(port).get("/api/meals?search=")
                .then()
                .statusCode(200)
                .and().body("name[0]",containsString("Bacon With Cheddar"))
                .and().body("name[1]",containsString("Carbonara with bacon"))
                .and().body("name[2]",containsString("Lasagna with BaCoN"));
    }


    @DisplayName("Add to cart - Successful")
    @Test
    @WithMockUser(username = "test1@mail.com")
    void whenAddToCartSuccessful_thenReturn200() throws Exception {

        mvc.perform(get("/api/account/cart/add?mealId=1").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("Added to Cart Successfully.")));
    }



}
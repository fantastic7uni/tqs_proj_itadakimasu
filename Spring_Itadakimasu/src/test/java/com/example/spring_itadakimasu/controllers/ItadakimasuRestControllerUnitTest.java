package com.example.spring_itadakimasu.controllers;

import com.example.spring_itadakimasu.entities.*;
import com.example.spring_itadakimasu.services.MealService;
import com.example.spring_itadakimasu.services.OrdersManager;
import com.example.spring_itadakimasu.services.UserManager;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import com.example.spring_itadakimasu.JsonUtils;
@SpringBootTest
@AutoConfigureMockMvc
class ItadakimasuRestControllerUnitTest {
    List<CartItem> userCart;
    List<Meal> meals;
    MealType pizza;
    MealType pasta;
    MealType dog;
    Meal m1;
    Meal m2;
    Meal m3;
    public static final MediaType APPLICATION_JSON_UTF8 =
            new MediaType(MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

    @Autowired
    private MockMvc mvc;

    @MockBean
    private MealService ms;

    @MockBean
    private UserManager userManager;

    @MockBean
    private OrdersManager orderManager;

    @BeforeEach
    void setUp() {
        meals = new ArrayList<>();
        userCart= new ArrayList<>();
        pizza = new MealType(1,"pizza");
        pasta = new MealType(2,"pasta");
        dog = new MealType(3,"hotdog");
    }



   // ########## MEAL SEARCHES ##########

    @DisplayName("Search Meals with Bacon and they exist")
    @Test
    void whenMealExists_thenReturnMeal() throws Exception {
        m1 = new Meal(1,pizza,"Bacon Pizza","Large bacon pizza with tomato sauce", 10, 10.0);
        meals.add(m1);

        when(ms.searchMeals(Mockito.anyString())).thenReturn(meals);

        mvc.perform(get("/api/meals?search=bacon"))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0]").value(hasKey("id")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0]").value(hasKey("mealType")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0]").value(hasKey("name")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0]").value(hasKey("description")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0]").value(hasKey("qtyAvailable")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0]").value(hasKey("price")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].name").value("Bacon Pizza"));
    }

    @DisplayName("Search Meals with asd and they dont exist")
    @Test
    void whenMealDoesntExist_thenReturn404() throws Exception {
        when(ms.searchMeals(Mockito.anyString())).thenReturn(meals);

        mvc.perform(get("/api/meals?search=asd"))
                .andExpect(status().isNotFound())
                .andExpect(content().string("Meals not found"));
    }

    @DisplayName("Search All Meals")
    @Test
    void whenGetAll_ReturnAll() throws Exception {
        m1 = new Meal(1,pizza,"Bacon Pizza","Large bacon pizza with tomato sauce", 10, 10.0);
        m2 = new Meal(2,pasta,"Carbonara","Medium carbonara with bechamel", 6, 11.6);
        m3 = new Meal(3,dog,"Cheese Hotdog","Small cheese hotdog with ketchup", 8, 8.2);
        meals.add(m1);
        meals.add(m2);
        meals.add(m3);

        // Mocks
        when(ms.searchMeals(Mockito.anyString())).thenReturn(meals);
        when(ms.getAll()).thenReturn(meals);

        //Test and Asserts
        mvc.perform(get("/api/meals"))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].name").value("Bacon Pizza"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.[1].name").value("Carbonara"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.[2].name").value("Cheese Hotdog"));

        mvc.perform(get("/api/meals?search="))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].name").value("Bacon Pizza"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.[1].name").value("Carbonara"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.[2].name").value("Cheese Hotdog"));
    }




    // ########## USER  ADD TO CART ##########

    @DisplayName("Add to cart - Successful")
    @Test
    void whenAddToCartSuccessful_thenReturn200() throws Exception {
        when(userManager.addToCart(anyInt())).thenReturn(0);

        mvc.perform(post("/api/account/cart/1"))
                .andExpect(status().isOk())
                .andExpect(content().string("Added to Cart Successfully."));
    }

    @DisplayName("Add to cart - Failed because of meal not found")
    @Test
    void whenAddToCartFailed1_thenReturn404() throws Exception {
        when(userManager.addToCart(anyInt())).thenReturn(1);

        mvc.perform(post("/api/account/cart/9999"))
                .andExpect(status().isNotFound())
                .andExpect(content().string("Something went wrong - Failed to find the meal to be added."));
    }

    @DisplayName("Add to cart - Failed because user is not logged in")
    @Test
    void whenAddToCartFailed2_thenReturn401() throws Exception {
        when(userManager.addToCart(anyInt())).thenReturn(2);

        mvc.perform(post("/api/account/cart/1"))
                .andExpect(status().isUnauthorized())
                .andExpect(content().string("You must login to add a product to your Cart."));
    }




    // ########## USER GET CART ##########

    @Test
    void whenGetCart_AndCartHas1Meal_thenReturn1Meal() throws Exception {
        // Mocks
        m1 = new Meal(1,pizza,"Bacon Pizza","Large bacon pizza with tomato sauce", 10, 10.0);
        CartItem cartItem = new CartItem();
        cartItem.setMeal(m1); cartItem.setQty(1);
        userCart.add(cartItem);
        when(userManager.getCart()).thenReturn(userCart);

        //Test
        mvc.perform(get("/api/account/cart"))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().json(JsonUtils.convertObjectToJsonString(userCart)));
    }


    @Test
    void whenGetCart_AndCartHas0Meal_thenReturnEmpty() throws Exception {
        when(userManager.getCart()).thenReturn(userCart);

        mvc.perform(get("/api/account/cart"))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().json(JsonUtils.convertObjectToJsonString(userCart)));
    }

    @Test
    void whenGetCart_And_UserNotLoggedIn_thenReturn401() throws Exception {
        when(userManager.getCart()).thenReturn(null);

        mvc.perform(get("/api/account/cart"))
                .andExpect(status().isUnauthorized())
                .andExpect(content().string("You must login first."));
    }




    // ########## USER REM FROM CART ##########

    @Test
    void whenRemFromCart_And_ItemExists_thenReturn200() throws Exception {
        when(userManager.RemFromCart(anyInt())).thenReturn(0);

        mvc.perform(delete("/api/account/cart/4"))
                .andExpect(status().isOk())
                .andExpect(content().string("Removed From Cart Successfully."));
    }

    @Test
    void whenRemFromCart_And_ItemNotFound_thenReturn404() throws Exception {
        when(userManager.RemFromCart(anyInt())).thenReturn(1);

        mvc.perform(delete("/api/account/cart/999"))
                .andExpect(status().isNotFound())
                .andExpect(content().string("Failed. You do not  have that item in your cart."));
    }



    @Test
    void whenRemFromCart_And_NotLoggedIn_thenReturn401() throws Exception {
        when(userManager.RemFromCart(anyInt())).thenReturn(2);

        mvc.perform(delete("/api/account/cart/4"))
                .andExpect(status().isUnauthorized())
                .andExpect(content().string("You must login first."));
    }



    // ########## ADDRESSES ADD ##########

    @Test
    void whenAddAddress_AndLoggedIn_thenReturn200() throws Exception {
        when(userManager.addAddress(anyObject())).thenReturn(0);

        //Get object ready
        Address anAddress = new Address("aa","bb","cc","123");
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson=ow.writeValueAsString(anAddress );

        //Perform post
        mvc.perform(post("/api/account/addresses").contentType(APPLICATION_JSON_UTF8)
                .content(requestJson))
                .andExpect(status().isOk())
                .andExpect(content().string("Address added successfully"));

    }

    @Test
    void whenAddAddress_AndNotLoggedIn_thenReturn401() throws Exception {
        when(userManager.addAddress(anyObject())).thenReturn(1);

        //Get object ready
        Address anAddress = new Address("aa","bb","cc","123");
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson=ow.writeValueAsString(anAddress);

        //Perform post
        mvc.perform(post("/api/account/addresses").contentType(APPLICATION_JSON_UTF8)
                .content(requestJson))
                .andExpect(status().isUnauthorized())
                .andExpect(content().string("You must login first."));
    }



    // ########## ADDRESSES REMOVE ##########

    @Test
    void whenRemAddress_AndLoggedIn_thenReturn200() throws Exception {
        when(userManager.deleteAddress(anyInt())).thenReturn(0);

        mvc.perform(delete("/api/account/addresses/10"))
                .andExpect(status().isOk())
                .andExpect(content().string("Removed Address Successfully."));
    }

    @Test
    void whenRemAddress_AndUserDoesNotHaveAddress_thenReturn404() throws Exception {
        when(userManager.deleteAddress(anyInt())).thenReturn(1);

        mvc.perform(delete("/api/account/addresses/10"))
                .andExpect(status().isNotFound())
                .andExpect(content().string("Failed. You do not  have that address."));
    }
    @Test
    void whenRemAddress_AndNotLoggedIn_thenReturn401() throws Exception {
        when(userManager.deleteAddress(anyInt())).thenReturn(2);

        mvc.perform(delete("/api/account/addresses/10"))
                .andExpect(status().isUnauthorized())
                .andExpect(content().string("You must login first."));
    }



    // ########## GET USER ADDRESSES ##########

    @Test
    void whenGetAddresses_AndLoggedIn_thenReturn401() throws Exception {
        // Mock
        List<Address> userAddresses = new ArrayList<>();
        Address address1 = new Address("aa","bb","cc","dd");
        userAddresses.add(address1);
        when(userManager.getAddresses()).thenReturn(userAddresses);

        //Test
        mvc.perform(get("/api/account/addresses"))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().json(JsonUtils.convertObjectToJsonString(userAddresses)));
    }

    @Test
    void whenGetAddresses_AndNotLoggedIn_thenReturn401() throws Exception {
        when(userManager.getAddresses()).thenReturn(null);

        mvc.perform(get("/api/account/addresses"))
                .andExpect(status().isUnauthorized())
                .andExpect(content().string("You must login first."));
    }

    @Test
    void assignOrderNull() throws Exception {
        when(orderManager.orderAssigned(new Orders())).thenReturn(null);
        mvc.perform(post("/api/deliveries/assigned").contentType(MediaType.APPLICATION_JSON).content("{}"))
                .andExpect(status().isBadRequest())
                .andExpect(content().string("Operation failed."));

    }

    @Test
    void assignOrder() throws Exception {
        when(orderManager.orderAssigned(any())).thenReturn(new Orders());
        mvc.perform(post("/api/deliveries/assigned").contentType(MediaType.APPLICATION_JSON).content("{}"))
                .andExpect(status().isOk());

    }

    @Test
    void checkoutSuccess() throws Exception {
        when(orderManager.addOrder(anyInt())).thenReturn(0);
        mvc.perform(get("/api/account/order/1"))
                .andExpect(status().isOk())
                .andExpect(content().string("Order Completed!"));
    }

    @Test
    void checkoutEmptyCart() throws Exception {
        when(orderManager.addOrder(anyInt())).thenReturn(1);
        mvc.perform(get("/api/account/order/1"))
                .andExpect(status().isNotFound())
                .andExpect(content().string("Your cart is empty!"));
    }

    @Test
    void checkoutNoLogin() throws Exception {
        when(orderManager.addOrder(anyInt())).thenReturn(2);
        mvc.perform(get("/api/account/order/1"))
                .andExpect(status().isUnauthorized())
                .andExpect(content().string("You must login to make an order."));
    }

    @Test
    void checkoutAddressNotFound() throws Exception {
        when(orderManager.addOrder(anyInt())).thenReturn(3);
        mvc.perform(get("/api/account/order/1"))
                .andExpect(status().isNotFound())
                .andExpect(content().string("Address not found."));
    }

    @Test
    void checkoutSendFailure() throws Exception {
        when(orderManager.addOrder(anyInt())).thenReturn(4);
        mvc.perform(get("/api/account/order/1"))
                .andExpect(status().isNotFound())
                .andExpect(content().string("Try again later. Failed to send order request to Delivery Service."));
    }

    @Test
    void checkoutFailure() throws Exception {
        when(orderManager.addOrder(anyInt())).thenReturn(-1);
        mvc.perform(get("/api/account/order/1"))
                .andExpect(status().isBadRequest())
                .andExpect(content().string("Operation failed."));
    }

    @Test
    void listOrdersEmpty() throws Exception{
        when(userManager.getOrders()).thenReturn(Collections.emptyList());
        mvc.perform(get("/api/account/orders"))
                .andExpect(status().isNotFound())
                .andExpect(content().string("You dont have previous orders!"));
    }

    @Test
    void listOrdersNoLogin() throws Exception{
        when(userManager.getOrders()).thenReturn(null);
        mvc.perform(get("/api/account/orders"))
                .andExpect(status().isUnauthorized())
                .andExpect(content().string("You must login to see your order."));
    }

    @Test
    void listOrders() throws Exception{
        List<Orders> orders = new ArrayList<>();
        Orders o1 = new Orders();
        orders.add(o1);

        when(userManager.getOrders()).thenReturn(orders);
        mvc.perform(get("/api/account/orders"))
                .andExpect(status().isOk());
    }

}
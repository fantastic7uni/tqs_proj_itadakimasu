package com.example.spring_itadakimasu.services;

import org.junit.jupiter.api.BeforeEach;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;


@SpringBootTest
@TestPropertySource(locations="classpath:test.properties") //isto serve para usar o ficheiro test.properties e não o application.properties (usa a BD de testes)
class MealServiceUnitTest{
    @BeforeEach
    void setUp() {

    }

}
package com.example.spring_itadakimasu.services;

import com.example.spring_itadakimasu.entities.*;
import com.example.spring_itadakimasu.repositories.OrdersRepository;
import com.example.spring_itadakimasu.repositories.UserRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

@SpringBootTest
@AutoConfigureMockMvc
class OrdersManagerUnitTest {
    User testUser;
    List<Orders> ordersList;
    Orders o1;
    Orders o2;

    public static final MediaType APPLICATION_JSON_UTF8 =
            new MediaType(MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));


    @Autowired
    private MockMvc mvc;

    @Autowired
    private OrdersManager orderManager;

    @MockBean
    private UserManager userManager;

    @MockBean
    private OrdersRepository orderRepository;

    @MockBean
    UserRepository userRep;

    @MockBean
    DeliveryService deliveryService;

    @BeforeEach
    void setUp() {
        // Mock Authenticated User
        testUser = new User();
        when(userRep.findByEmail(anyString())).thenReturn(testUser);

        ordersList = new ArrayList<>();
        Orders o1 = new Orders();
        o1.setOrderStatus(OrderStatus.WAITING);
        o1.setQtyOrdered(2);
        o1.setTotalPrice("10");
        o1.setAddress("Av 25 de Abril");
        o1.setId(0);
        o1.setRiderId(0);
        o1.setRiderName("Alberto");
        Orders o2 = new Orders();
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void getAll() {
        when(orderRepository.findAll()).thenReturn(ordersList);
        assertEquals(orderManager.getAll(),ordersList);
    }

    @Test
    void getOrdersById() {
        when(orderRepository.findById(anyInt())).thenReturn(o1);
        assertEquals(orderManager.getOrdersById(0),o1);
    }

    @Test
    void addOrderNotLogged() {
        when(userManager.getUserLoggedIn()).thenReturn(null);
        assertEquals(2,orderManager.addOrder(0));
    }

    @Test
    void addOrderCartEmpty() {
        when(userManager.getUserLoggedIn()).thenReturn(testUser);
        when(userManager.getCart()).thenReturn(Collections.emptyList());
        assertEquals(1,orderManager.addOrder(0));

    }

    @Test
    void addOrderAddressNull() {
        List<CartItem> cart = new ArrayList<>();
        cart.add(new CartItem());

        when(userManager.getUserLoggedIn()).thenReturn(testUser);
        when(userManager.getCart()).thenReturn(cart);
        assertEquals(3,orderManager.addOrder(0));
    }

    @Test
    void addOrder(){
        List<CartItem> cart = new ArrayList<>();
        CartItem ci = new CartItem();
        ci.setQty(1);
        Meal m = new Meal();
        m.setPrice(1);
        ci.setMeal(m);
        cart.add(ci);

        List<Address> addresses = new ArrayList<>();
        Address a = Mockito.mock(Address.class);
        addresses.add(a);

        testUser.setAddresses(addresses);

        when(userManager.getUserLoggedIn()).thenReturn(testUser);
        when(userManager.getCart()).thenReturn(cart);
        when(a.getAddressId()).thenReturn(0);
        when(a.getAddress()).thenReturn("test1");

        assertEquals(0,orderManager.addOrder(0));
    }

    @Test
    void orderAssigned() {
        Orders o = Mockito.mock(Orders.class);
        when(o.getId()).thenReturn(1);
        when(orderRepository.findById(o.getId())).thenReturn(o);

        assertEquals(o,orderManager.orderAssigned(o));
    }

    @Test
    void orderAssignedDelivered(){
        Orders o = Mockito.mock(Orders.class);
        when(o.getId()).thenReturn(1);
        when(o.getOrderStatus()).thenReturn(OrderStatus.DELIVERED);
        when(orderRepository.findById(o.getId())).thenReturn(o);

        assertEquals(o,orderManager.orderAssigned(o));
    }
}
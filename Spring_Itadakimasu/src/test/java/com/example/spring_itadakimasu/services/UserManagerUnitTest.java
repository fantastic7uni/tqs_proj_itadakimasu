package com.example.spring_itadakimasu.services;

import com.example.spring_itadakimasu.entities.*;
import com.example.spring_itadakimasu.repositories.CartItemRepository;
import com.example.spring_itadakimasu.repositories.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.security.test.context.support.WithAnonymousUser;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

@SpringBootTest
class UserManagerUnitTest {
    User testUser;
    MealType pizza;
    Meal m1;
    CartItem item1;

    @Autowired
    UserManager userManager;

    @MockBean
    MealService mealService;

    @MockBean
    UserRepository userRep;

    @MockBean
    CartItemRepository cartRep;

    @BeforeEach
    void setUp() {
        pizza = new MealType(1,"pizza");

        // Mock Authenticated User
        testUser = new User();
        when(userRep.findByEmail(anyString())).thenReturn(testUser);
 }


    // ########## ADD TO CART TESTS ##########

    @Test
    @WithAnonymousUser
    void whenAddToCartSuccessful_Return0(){

        // Mocks
        m1 = new Meal(1,pizza,"Bacon Pizza","Large bacon pizza with tomato sauce", 10, 10.0);
        item1 = new CartItem(m1,1);
        when(mealService.existsById(anyInt())).thenReturn(true);
        when(mealService.getMealById(anyInt())).thenReturn(m1);

        // Test & Assertions
        assertThat(testUser.getCart(), hasSize(0));
        int result = userManager.addToCart(0);
        assertThat(result, is(0));
        assertThat(testUser.getCart(),hasSize(1));
        assertThat(testUser.getCart(), hasItem(allOf(
                hasProperty("meal", is(m1)),
                hasProperty("qty", is(1)))));

    }

    @Test
    @WithAnonymousUser
    void whenAddToCart_AndAlreadyExists_thenIncrementQty(){
        // Mocks
        m1 = new Meal(1,pizza,"Bacon Pizza","Large bacon pizza with tomato sauce", 10, 10.0);
        item1 = new CartItem(m1,1);
        testUser.getCart().add(item1); // m1 in cart
        when(mealService.existsById(anyInt())).thenReturn(true);
        when(mealService.getMealById(anyInt())).thenReturn(m1);

        // Test & Assertions
        assertThat(testUser.getCart(),hasSize(1));
        int result = userManager.addToCart(0);
        assertThat(result, is(0));
        assertThat(testUser.getCart(),hasSize(1));
        assertThat(testUser.getCart(),hasItem(item1));
        assertThat(testUser.getCart(), hasItem(allOf(
                hasProperty("meal", is(m1)),
                hasProperty("qty", is(2)))));
    }

    @Test
    @WithAnonymousUser
    void whenAddToCart_And_MealNotFound_Return1(){
        // Mocks
        when(mealService.existsById(anyInt())).thenReturn(false);

        // Test & Assertions
        assertThat(testUser.getCart(),hasSize(0));
        int result = userManager.addToCart(999999);
        assertThat(result, is(1));
        assertThat(testUser.getCart(),hasSize(0));
    }

    @Test
    void whenAddToCart_And_UserNotLogged_Return2(){
        // Test & Assertions
        assertThat(testUser.getCart(),hasSize(0));
        int result = userManager.addToCart(999999);
        assertThat(result, is(2));
        assertThat(testUser.getCart(),hasSize(0));
    }


    
    // ########## REMOVE FROM CART TESTS ##########

    @Test
    @WithAnonymousUser
    void whenRemFromCart_And_ItemIsFound_RemovedSuccessfully(){
        // Mocks
        m1 = new Meal(1,pizza,"Bacon Pizza","Large bacon pizza with tomato sauce", 10, 10.0);
        item1 = new CartItem(m1,1);
        when(mealService.existsById(anyInt())).thenReturn(true);
        when(mealService.getMealById(anyInt())).thenReturn(m1);

        // Test & Assertions
        assertThat(testUser.getCart(),hasSize(0));
        userManager.addToCart(1);
        assertThat(testUser.getCart(),hasSize(1));

        int result = userManager.RemFromCart(item1.getId());
        assertThat(testUser.getCart(),hasSize(0));
        assertThat(result, is(0));
    }

    @Test
    @WithAnonymousUser
    void whenRemFromCart_And_ItemNotFound_Return1(){
        int result = userManager.RemFromCart(9999999);
        assertThat(result, is(1));
    }

    @Test
    void whenRemFromCart_And_NotLoggedIn_Return2(){
        int result = userManager.RemFromCart(1);
        assertThat(result, is(2));
    }



    // ##########  ADD ADDRESS TESTS ##########


    @Test
    @WithAnonymousUser
    void whenAddAddress_AndLoggedIn_Return0(){
        Address anAddress = new Address("aa","bb","cc","123");

        // Test & Assertions
        assertThat(testUser.getAddresses(), hasSize(0));
        var result = userManager.addAddress(anAddress);
        System.out.println(result);
        assertThat(result, is(0));
        assertThat(testUser.getAddresses(),hasSize(1));
        assertThat(testUser.getAddresses(), hasItem(allOf(
                hasProperty("country", is("aa")),
                hasProperty("district", is("bb")),
                hasProperty("address", is("cc")),
                hasProperty("zipCode", is("123"))
                )));
    }

    @Test
    void whenAddAddress_AndNotLoggedIn_Return2(){
        Address anAddress = new Address("aa","bb","cc","123");
        int result = userManager.addAddress(anAddress);
        assertThat(result, is(1));
    }



    // ##########  REMOVE ADDRESS TESTS ##########

    @Test
    void whenRemoveAddress_AndNotLoggedIn_Return2(){

        Address anAddress = new Address("aa","bb","cc","123");
        anAddress.setAddressId(12345);
        testUser.getAddresses().add(anAddress);

        int result = userManager.deleteAddress(12345);
        assertThat(result, is(2));
    }

    @Test
    @WithAnonymousUser
    void whenRemoveAddress_AndLoggedIn_AndAddressExists_Return2(){

        Address anAddress = new Address("aa","bb","cc","123");
        anAddress.setAddressId(12345);
        testUser.getAddresses().add(anAddress);

        int result = userManager.deleteAddress(12345);
        assertThat(result, is(0));
    }

    @Test
    @WithAnonymousUser
    void whenRemoveAddress_AndLoggedIn_AndAddressDoesNotExist_Return2(){

        Address anAddress = new Address("aa","bb","cc","123");
        anAddress.setAddressId(12345);
        testUser.getAddresses().add(anAddress);

        int result = userManager.deleteAddress(123456789);
        assertThat(result, is(1));
    }

}

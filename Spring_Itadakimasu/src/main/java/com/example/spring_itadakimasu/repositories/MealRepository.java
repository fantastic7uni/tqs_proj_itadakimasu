package com.example.spring_itadakimasu.repositories;

import com.example.spring_itadakimasu.entities.Meal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MealRepository extends JpaRepository<Meal, Integer> {
    List<Meal> findAll();
    List<Meal> findMealByNameContaining(String name);
    Meal findMealById(int id);
}

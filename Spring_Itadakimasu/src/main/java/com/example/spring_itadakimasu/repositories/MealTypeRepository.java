package com.example.spring_itadakimasu.repositories;

import com.example.spring_itadakimasu.entities.MealType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MealTypeRepository extends JpaRepository<MealType, Long> {
}

package com.example.spring_itadakimasu.entities;

public enum OrderStatus {
    WAITING,ACCEPTED,REFUSED,DELIVERED
}

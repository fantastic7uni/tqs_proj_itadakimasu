package com.example.spring_itadakimasu.entities;


import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import javax.persistence.*;
import java.security.SecureRandom;
import java.util.*;

@Entity
public class Orders {

    // Attributes
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @OnDelete(action= OnDeleteAction.CASCADE)
    @OneToMany(targetEntity = OrderItem.class, fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "order_item__id")
    private List<OrderItem> qtyMeals = new ArrayList<>();

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy HH:mm:ss")
    private Date date;
    private int qtyOrdered;
    private String totalPrice;
    private OrderStatus orderStatus;
    private String address;
    private int riderId;
    private String riderName;
    private String riderContact;
    private double lat; //rider's latitude
    private double lon; //rider's longitude

    public Orders() {
        var r = new SecureRandom();
        this.lat = 37.02042418035774 + r.nextDouble() * 4;
        this.lon = -9.03289 + r.nextDouble() * 2.5;
    }

    // Getters & Setters
    public int getId() { return id; }
    public void setId(int id) { this.id = id; }

    public List<OrderItem> getQtyMeals() { return qtyMeals; }
    public void setQtyMeals(List<OrderItem> qtyMeals) { this.qtyMeals = qtyMeals; }

    public int getQtyOrdered() { return qtyOrdered; }
    public void setQtyOrdered(int qtyOrdered) { this.qtyOrdered = qtyOrdered; }

    public String getTotalPrice() { return totalPrice; }
    public void setTotalPrice(String totalPrice) { this.totalPrice = totalPrice; }

    public OrderStatus getOrderStatus() { return orderStatus; }
    public void setOrderStatus(OrderStatus orderStatus) { this.orderStatus = orderStatus; }

    public Date getDate() { return date; }
    public void setDate(Date date) { this.date = date; }

    public String getAddress() { return address; }
    public void setAddress(String address) { this.address = address; }
    
    public int getRiderId() { return riderId; }
    public void setRiderId(int riderId) { this.riderId = riderId; }

    public String getRiderName() { return riderName; }
    public void setRiderName(String riderName) { this.riderName = riderName; }

    public String getRiderContact() { return riderContact; }
    public void setRiderContact(String riderContact) { this.riderContact = riderContact; }

    public double getLat() { return lat; }
    public void setLat(double lat) { this.lat = lat; }

    public double getLon() { return lon; }
    public void setLon(double lon) { this.lon = lon; }
}



package com.example.spring_itadakimasu.entities;

import javax.persistence.*;

@Entity
public class Address {

    // Attributes
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int addressId;

    private String country;
    private String district;
    private String address;
    private String zipCode;

    public Address(){/* Constructor */}

    public Address(String country, String district, String address, String zipCode) {
        this.country=country;
        this.district=district;
        this.address=address;
        this.zipCode=zipCode;
    }

    // Getters and Setters

    public int getAddressId() { return addressId; }
    public void setAddressId(int id) { this.addressId = id; }

    public String getCountry() { return country; }
    public void setCountry(String country) { this.country = country; }

    public String getDistrict() { return district; }
    public void setDistrict(String district) { this.district = district; }

    public String getAddress() { return address; }
    public void setAddress(String address) { this.address = address; }

    public String getZipCode() { return zipCode; }
    public void setZipCode(String zipCode) { this.zipCode = zipCode; }
}

package com.example.spring_itadakimasu.entities;

import javax.persistence.*;

@Entity
public class CartItem {

    // Attributes
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne(targetEntity = Meal.class, fetch = FetchType.EAGER)
    @JoinColumn(name = "meal_id")
    private Meal meal;
    private int qty;

    public CartItem(){ /* Constructor */}

    public CartItem(Meal meal, int qty){
        this.meal = meal;
        this.qty = qty;
    }

    // Getters And Setters
    public int getId() { return id; }
    public void setId(int id) { this.id = id; }

    public Meal getMeal() { return meal; }
    public void setMeal(Meal meal) { this.meal = meal; }

    public int getQty() { return qty; }
    public void setQty(int qty) { this.qty = qty; }
}

package com.example.spring_itadakimasu.entities;

import javax.persistence.*;

@Entity
public class Meal {

    // Attributes
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @ManyToOne(targetEntity = MealType.class, fetch = FetchType.EAGER)
    @JoinColumn(name = "meal_type_id")
    private MealType mealType;

    private String name;
    private String description;
    private int qtyAvailable;
    private double price;

    public Meal(){/* Constructor */}

    public Meal(int id, MealType mealType, String name, String description, int qtyAvailable, double price) {
        this.id = id;
        this.mealType = mealType;
        this.name = name;
        this.description = description;
        this.qtyAvailable = qtyAvailable;
        this.price = price;
    }
    public Meal(MealType mealType, String name, String description, int qtyAvailable, double price) {
        this.mealType = mealType;
        this.name = name;
        this.description = description;
        this.qtyAvailable = qtyAvailable;
        this.price = price;
    }

    public int getId() { return id; }
    public void setId(int id) { this.id = id; }

    public MealType getMealType() { return mealType; }
    public void setMealType(MealType mealType) { this.mealType = mealType; }

    public String getName() { return name; }
    public void setName(String name) { this.name = name; }

    public String getDescription() { return description; }
    public void setDescription(String description) { this.description = description; }

    public int getQtyAvailable() { return qtyAvailable; }
    public void setQtyAvailable(int qtyAvailable) { this.qtyAvailable = qtyAvailable; }

    public double getPrice() { return price; }
    public void setPrice(double price) { this.price = price; }
}

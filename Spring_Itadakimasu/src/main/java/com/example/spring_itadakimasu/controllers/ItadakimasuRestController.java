package com.example.spring_itadakimasu.controllers;

import com.example.spring_itadakimasu.entities.Address;
import com.example.spring_itadakimasu.entities.CartItem;
import com.example.spring_itadakimasu.entities.Meal;
import com.example.spring_itadakimasu.entities.Orders;
import com.example.spring_itadakimasu.services.MealService;
import com.example.spring_itadakimasu.services.OrdersManager;
import com.example.spring_itadakimasu.services.UserManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ScheduledFuture;

@Controller
@ResponseBody
@RequestMapping("/api")
public class ItadakimasuRestController {
    private static final String FAILED = "Operation failed.";
    private static final String MUST_LOGIN = "You must login first.";
    @Autowired
    MealService ms;

    @Autowired
    UserManager userManager;

    @Autowired
    OrdersManager orderManager;

    @Autowired
    TaskScheduler taskScheduler;

    List<Integer> ridersTracked = new ArrayList<>();
    ScheduledFuture<?> scheduledFuture;

    /* ADDRESSES ENDPOINTS */

    @GetMapping("/account/addresses")
    public ResponseEntity<Object> getAddresses(){
        var addresses = userManager.getAddresses();
        if(addresses== null){
            return new ResponseEntity<>(MUST_LOGIN, HttpStatus.UNAUTHORIZED);
        }
        return new ResponseEntity<>(addresses, HttpStatus.OK);
    }

    @PostMapping("/account/addresses")
    public ResponseEntity<Object> createAddress(@RequestBody Address address) {
        int operationStatus=userManager.addAddress(address);
        if(operationStatus==0){
            return new ResponseEntity<>("Address added successfully",HttpStatus.OK);
        }
        return new ResponseEntity<>(MUST_LOGIN,HttpStatus.UNAUTHORIZED);
    }

    @DeleteMapping("/account/addresses/{id}")
    public ResponseEntity<Object> deleteAddress(@PathVariable int id)  {
        int operationStatus = userManager.deleteAddress(id);

        if(operationStatus==0)
            return new ResponseEntity<>("Removed Address Successfully.", HttpStatus.OK);
        else if(operationStatus==1)
            return new ResponseEntity<>("Failed. You do not  have that address.", HttpStatus.NOT_FOUND);
        else if(operationStatus==2)
            return new ResponseEntity<>(MUST_LOGIN, HttpStatus.UNAUTHORIZED);

        return new ResponseEntity<>(FAILED, HttpStatus.BAD_REQUEST);
    }

    @GetMapping("/meals")
    public ResponseEntity<Object> getMeals(@RequestParam(required = false) String search) {
        List<Meal> meals;
        if(search!=null && !search.isEmpty())
            meals = ms.searchMeals(search);
        else
            meals = ms.getAll();

        if(meals.isEmpty())
            return new ResponseEntity<>("Meals not found", HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(meals, HttpStatus.OK);
    }

    /* USER CART  ENDPOINTS */

    @GetMapping("/account/cart")
    public ResponseEntity<Object> getCart() {
        List<CartItem> userCart = userManager.getCart();
        if(userCart!=null)
            return new ResponseEntity<>(userCart, HttpStatus.OK);

        return new ResponseEntity<>(MUST_LOGIN, HttpStatus.UNAUTHORIZED);
    }

    @PostMapping("/account/cart/{mealId}")
    public ResponseEntity<Object> addToCart(@PathVariable  int mealId) {
        int operationStatus = userManager.addToCart(mealId);

        if(operationStatus==0)
            return new ResponseEntity<>("Added to Cart Successfully.", HttpStatus.OK);
        else if(operationStatus==1)
            return new ResponseEntity<>("Something went wrong - Failed to find the meal to be added.", HttpStatus.NOT_FOUND);
        else if(operationStatus==2)
            return new ResponseEntity<>("You must login to add a product to your Cart.", HttpStatus.UNAUTHORIZED);

        return new ResponseEntity<>(FAILED, HttpStatus.BAD_REQUEST);
    }

    @DeleteMapping("/account/cart/{cartItemId}") // remove/4 em vez de remove?cartItemId=4
    public ResponseEntity<Object> remFromCart(@PathVariable int cartItemId) {
        int operationStatus = userManager.RemFromCart(cartItemId);

        if(operationStatus==0)
            return new ResponseEntity<>("Removed From Cart Successfully.", HttpStatus.OK);
        else if(operationStatus==1)
            return new ResponseEntity<>("Failed. You do not  have that item in your cart.", HttpStatus.NOT_FOUND);
        else if(operationStatus==2)
            return new ResponseEntity<>(MUST_LOGIN, HttpStatus.UNAUTHORIZED);

        return new ResponseEntity<>(FAILED, HttpStatus.BAD_REQUEST);
    }

    @GetMapping("/account/cart/clear")
    public ResponseEntity<Object> clearCart(){
        if(userManager.clearCart()){
            return new ResponseEntity<>("Cart cleared!", HttpStatus.OK);
        }
        else{
            return new ResponseEntity<>(FAILED, HttpStatus.BAD_REQUEST);
        }

    }

    /* ORDERS  ENDPOINTS */

    @PostMapping(value="/deliveries/assigned")
    public ResponseEntity<Object> deliveryAssigned(@RequestBody Orders o){
        var assignedOrder = orderManager.orderAssigned(o);
        if(assignedOrder!=null) {
            return new ResponseEntity<>(assignedOrder, HttpStatus.OK);
        }
        return new ResponseEntity<>(FAILED,HttpStatus.BAD_REQUEST);
    }

    @GetMapping("/account/order/{addressId}")
    public ResponseEntity<Object> doCheckout(@PathVariable int addressId){
        int operationStatus = orderManager.addOrder(addressId);

        if(operationStatus==0){
            userManager.clearCart();
            return new ResponseEntity<>("Order Completed!", HttpStatus.OK);
        }
        else if (operationStatus == 1)
            return new ResponseEntity<>("Your cart is empty!", HttpStatus.NOT_FOUND);
        else if (operationStatus == 2)
            return new ResponseEntity<>("You must login to make an order.", HttpStatus.UNAUTHORIZED);
        else if (operationStatus == 3)
            return new ResponseEntity<>("Address not found.", HttpStatus.NOT_FOUND);
        else if (operationStatus == 4)
            return new ResponseEntity<>("Try again later. Failed to send order request to Delivery Service.", HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(FAILED, HttpStatus.BAD_REQUEST);
    }


    @GetMapping("/account/orders")
    public ResponseEntity<Object> listOrders(){
        List<Orders> orders = userManager.getOrders();
        if(orders!=null){
            if(orders.isEmpty())
                return new ResponseEntity<>("You dont have previous orders!", HttpStatus.NOT_FOUND);
            return new ResponseEntity<>(orders, HttpStatus.OK);
        }
        return new ResponseEntity<>("You must login to see your order.", HttpStatus.UNAUTHORIZED);
    }

}


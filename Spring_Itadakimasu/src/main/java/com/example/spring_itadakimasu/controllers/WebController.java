package com.example.spring_itadakimasu.controllers;

import com.example.spring_itadakimasu.UserPrincipal;
import com.example.spring_itadakimasu.entities_dto.UserDTO;
import com.example.spring_itadakimasu.services.OrdersManager;
import com.example.spring_itadakimasu.services.UserManager;
import org.apache.catalina.Manager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Controller
public class WebController {

    @Autowired
    UserManager userManager;

    @Autowired
    OrdersManager orderManager;

    private Model getModel(Model model){ // this function updates the model to have logged in user info
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal(); //get the user's data if they are logged in

        if (principal instanceof UserDetails) {
            String firstName = ((UserPrincipal)principal).getfirstName(); //add the relevant user info to the model for thymeleaf to access in the html
            model.addAttribute("userFirstName", firstName);
            model.addAttribute("loggedin", true);
        }else{
            model.addAttribute("loggedin", false);
        }
        return model;
    }

    @GetMapping("/")
    public ModelAndView home(Model model){
        return new ModelAndView("home", (Map<String, ?>) getModel(model));
    }

    @GetMapping("/account/login")
    public ModelAndView login(){
        return new ModelAndView("login");
    }

    @GetMapping("/account/register")
    public ModelAndView register(WebRequest request, Model model){
        model.addAttribute("user", new UserDTO());
        return new ModelAndView("register", (Map<String, ?>) getModel(model));
    }

    @PostMapping("/account/do_register")
    public ModelAndView processRegister(@ModelAttribute("user") UserDTO user,
                                  HttpServletRequest request,
                                  Errors errors) {
        try {
            userManager.registerUser(user);
        } catch (Exception e) {
            var mav = new ModelAndView("register");
            mav.addObject("message", "An account for that email or with that phone number already exists.");
            return mav;
        }

        return new ModelAndView("login");
    }

    @GetMapping("/order/checkout")
    public ModelAndView checkout(Model model){
        return new ModelAndView("checkout", (Map<String, ?>) getModel(model));
    }

    @GetMapping("/meals")
    public ModelAndView food(Model model){
        return new ModelAndView("food", (Map<String, ?>) getModel(model));
    }

    @GetMapping("/order/details")
    public ModelAndView order(Model model) {return new ModelAndView("order-details", (Map<String, ?>) getModel(model));}

    @GetMapping("/account/address/add")
    public ModelAndView addAddress(Model model) {
        return new ModelAndView("add-address", (Map<String, ?>) getModel(model));
    }

    @GetMapping("/account/orders")
    public ModelAndView past(Model model){return new ModelAndView("orders", (Map<String, ?>) getModel(model));}


    @GetMapping("/account/address")
    public ModelAndView editUserAddress(Model model) {
        return new ModelAndView("list-address", (Map<String, ?>) getModel(model));
    }

    @GetMapping("/account/trackorder/{id}")
    public ModelAndView map(Model model,@PathVariable int id){
        ModelAndView mv = new ModelAndView("map",(Map<String, ?>) getModel(model));
        var order = orderManager.getOrdersById(id);
        System.out.println(order.getRiderName());
        mv.addObject("order",order);
        return mv;
    }
}

package com.example.spring_itadakimasu.entities_dto;

public class UserDTO {
    private int id;

    private String emailDto;
    private String passwordDto;
    private String firstNameDto;
    private String lastNameDto;
    private String phoneNumberDto;

    public UserDTO(){ /* Constructor */ }

    public UserDTO(String emailDto, String passwordDto, String firstnameDto, String lastnameDto, String phoneNumberDto){
        this.emailDto=emailDto;
        this.passwordDto=passwordDto;
        this.firstNameDto=firstnameDto;
        this.lastNameDto=lastnameDto;
        this.phoneNumberDto=phoneNumberDto;
    }

    // Getters & Setters
    public int getId() { return id; }
    public void setId(int id) { this.id = id; }

    public String getEmail() { return emailDto; }
    public void setEmail(String email) { this.emailDto = email; }

    public String getPassword() { return passwordDto; }
    public void setPassword(String password) { this.passwordDto = password; }

    public String getFirstName() { return firstNameDto; }
    public void setFirstName(String firstName) { this.firstNameDto = firstName; }

    public String getLastName() { return lastNameDto; }
    public void setLastName(String lastName) { this.lastNameDto = lastName; }

    public String getPhoneNumber() { return phoneNumberDto; }
    public void setPhoneNumber(String phoneNumber) { this.phoneNumberDto = phoneNumber; }
}

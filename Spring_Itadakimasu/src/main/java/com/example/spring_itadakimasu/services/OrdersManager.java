package com.example.spring_itadakimasu.services;

import com.example.spring_itadakimasu.entities.*;
import com.example.spring_itadakimasu.repositories.OrdersRepository;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

@Service
public class OrdersManager {
    //private static final String SERVER = "http://localhost:8081";
    private static final String SERVER = "http://192.168.160.223:8081";

    DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    
    @Autowired
    private OrdersRepository orderRep;

    @Autowired
    private UserManager userManager;

    @Autowired
    private DeliveryService deliveryService;

    public List<Orders> getAll(){return orderRep.findAll();}
    public Orders getOrdersById(int id){return orderRep.findById(id);}

    Logger logger = Logger.getLogger(OrdersManager.class.getName());

    public int addOrder(int addressId){
        var userLoggedIn = userManager.getUserLoggedIn();
        var cart = userManager.getCart();
        var productsJSON = new StringBuilder("[");

        if(userLoggedIn==null) return 2; //Not logged
        if(cart.isEmpty()) return 1; //Cart was empty
        
        var date = new Date();
        var o = new Orders();

        for(Address addr : userLoggedIn.getAddresses())
            if(addr.getAddressId() == addressId)
                o.setAddress(addr.getAddress());
        if(o.getAddress()==null || o.getAddress().equals("") )
            return 3;
        try {
            o.setDate(dateFormat.parse(dateFormat.format(date)));
            var qtyOrdered=0;
            var totalPrice = 0.0;
            for(CartItem item : cart){
                var newItem = new OrderItem(item.getMeal(),item.getQty());
                o.getQtyMeals().add(newItem);
                qtyOrdered+=item.getQty();
                totalPrice += item.getQty()*item.getMeal().getPrice();
                if(productsJSON.length()!=1) productsJSON.append(",");
                productsJSON.append("{ \"name\": \"").append(item.getMeal().getName()).append("\", \"qty\": ").append(item.getQty()).append("}");
            }

            o.setQtyOrdered(qtyOrdered);
            o.setTotalPrice(totalPrice+"");
            o.setOrderStatus(OrderStatus.WAITING);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        userLoggedIn.getOrders().add(o);
        orderRep.save(o);
        userManager.save(userLoggedIn);


        //send the order to Vroom API
        try {
            var requestJSON = new JSONObject();
            productsJSON.append("]");
            requestJSON.put("products", new JSONArray(productsJSON.toString()));
            requestJSON.put("date", dateFormat.format(date));
            requestJSON.put("shop","Itadakimasu");
            requestJSON.put("shopOrderId", o.getId());
            requestJSON.put("orderStatus", "WAITING");
            requestJSON.put("total_price", o.getTotalPrice());
            requestJSON.put("address", o.getAddress());
            requestJSON.put("client", userLoggedIn.getFirstName() + " " + userLoggedIn.getLastName());
            requestJSON.put("clientContact", userLoggedIn.getPhoneNumber());
            requestJSON.put("destinationLat", o.getLat());
            requestJSON.put("destinationLong", o.getLon());

            deliveryService.postOrder(requestJSON);
        }catch(Exception e){
            e.printStackTrace();
            return 4;
        }
        return 0; //Made Order
    }
    
    // 
    //MAP

    public Orders orderAssigned(Orders o){
        try {

            var assignedOrder = orderRep.findById(o.getId()); //finds the order with the specific id

            if(o.getOrderStatus() == OrderStatus.DELIVERED){
                assignedOrder.setOrderStatus(OrderStatus.DELIVERED);
                orderRep.save(assignedOrder);
                return assignedOrder;
            }

            System.out.println(o.getRiderId());
            assignedOrder.setRiderId(o.getRiderId());
            assignedOrder.setRiderName(o.getRiderName());
            assignedOrder.setRiderContact(o.getRiderContact());
            assignedOrder.setOrderStatus(o.getOrderStatus());

            orderRep.save(assignedOrder); //save changes to the order
            return assignedOrder;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}

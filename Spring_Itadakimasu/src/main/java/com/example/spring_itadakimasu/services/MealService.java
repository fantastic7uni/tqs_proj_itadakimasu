package com.example.spring_itadakimasu.services;
import com.example.spring_itadakimasu.entities.Meal;
import com.example.spring_itadakimasu.entities.MealType;
import com.example.spring_itadakimasu.repositories.MealRepository;
import com.example.spring_itadakimasu.repositories.MealTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MealService {

    @Autowired
    private MealRepository mr;

    public List<Meal> searchMeals(String search){ return mr.findMealByNameContaining(search); }
    public List<Meal> getAll(){ return mr.findAll(); }
    public Meal getMealById(int id ) { return mr.findMealById(id); }
    public boolean existsById(int id){return mr.existsById(id);}
}

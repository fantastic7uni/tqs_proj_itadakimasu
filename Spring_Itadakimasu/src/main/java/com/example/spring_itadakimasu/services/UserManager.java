package com.example.spring_itadakimasu.services;

import com.example.spring_itadakimasu.UserPrincipal;
import com.example.spring_itadakimasu.entities.*;
import com.example.spring_itadakimasu.entities_dto.UserDTO;
import com.example.spring_itadakimasu.exception.ResourceNotFoundException;
import com.example.spring_itadakimasu.repositories.AddressRepository;
import com.example.spring_itadakimasu.repositories.CartItemRepository;
import com.example.spring_itadakimasu.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class UserManager implements UserDetailsService{

    @Autowired
    private UserRepository userRep;

    @Autowired
    private CartItemRepository cartRep;

    @Autowired
    private MealService mealService;

    @Autowired
    AddressRepository addressRep;

    public void registerUser(UserDTO user) throws ResourceNotFoundException {
        if (emailPhoneExist(user.getEmail(),user.getPhoneNumber())) {
            throw new ResourceNotFoundException("There is an account with that email address: "
                    + user.getEmail());
        }
        var userPersistent = new User(user.getEmail(), user.getPassword(), user.getFirstName(), user.getLastName(), user.getPhoneNumber());
        userRep.save(userPersistent);
    }

    private boolean emailPhoneExist(String email, String phoneNumber) {
        return ((userRep.findByEmail(email) != null)||(userRep.findByPhoneNumber(phoneNumber) != null));
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        var user = userRep.findByEmail(email);
        if(user==null)
            throw new UsernameNotFoundException("User not found");

        return new UserPrincipal(user); //return a UserPrincipal with the found user
    }

    //User's addresses

    public int addAddress(Address address){
        User user = getUserLoggedIn();
        if(user!=null){
            user.getAddresses().add(address);
            userRep.save(user);
            return 0 ;
        }
        return 1 ;
    }

    public int deleteAddress(int id) {
        var userLoggedIn = getUserLoggedIn();
        if(userLoggedIn!=null){
            for(Address addr : userLoggedIn.getAddresses()){
                if(addr.getAddressId() == id){
                    userLoggedIn.getAddresses().remove(addr);
                    userRep.save(userLoggedIn);
                    return 0; //Found addr and removed successfully
                }
            }
            return 1; //Failed to find addr
        }
        return 2; // Must Login first

    }

    public List<Address> getAddresses(){
        var user = getUserLoggedIn();
        if(user!=null){
            return getUserLoggedIn().getAddresses();
        }
        else{
            return null;
        }   

    }


    public int addToCart(int mealId) {
        User userLoggedIn = getUserLoggedIn();
        if(userLoggedIn!=null){
            if(mealService.existsById(mealId)){
                Meal m = mealService.getMealById(mealId);

                // If he already has the meal => increment qty by 1
                for (CartItem item : userLoggedIn.getCart()){
                    if( item.getMeal().toString().equals(m.toString()) ){
                        item.setQty(item.getQty()+1);
                        cartRep.save(item);
                        return 0;
                    }
                }

                // Else - Create New Item
                CartItem newItem = new CartItem(m,1);
                userLoggedIn.getCart().add(newItem);
                userRep.save(userLoggedIn);
                return 0; //Found meal and added to cart
            }
                return 1; //Failed to find meal
        }
            return 2; // Must Login first
    }


    public List<CartItem> getCart() {
        User userLoggedIn= getUserLoggedIn();

        if(userLoggedIn!=null){
            return userLoggedIn.getCart();
        }
        return null;
    }


    public int RemFromCart(int cartItemId) {
        User userLoggedIn = getUserLoggedIn();
        if(userLoggedIn!=null){
            for(CartItem item : userLoggedIn.getCart()){
                if(item.getId() == cartItemId){
                    userLoggedIn.getCart().remove(item);
                    userRep.save(userLoggedIn);
                    return 0; //Found item and removed successfully
                }
            }
            return 1; //Failed to find meal
        }
        return 2; // Must Login first
    }

    public User getUserLoggedIn(){
        User user;
        String email=null;

        if(SecurityContextHolder.getContext().getAuthentication()!=null){

            Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            if (principal instanceof UserPrincipal)
                email = ((UserPrincipal)principal).getEmail();
            if (principal instanceof UserDetails)
                email = ((UserDetails) principal).getUsername();
            if(principal instanceof String)
                email = principal.toString();

            //User user= repository.findById(roomID).orElseThrow(() -> new ResourceNotFoundException("Room not found for this id :: " + roomID));
            user = userRep.findByEmail(email);
            return user;
        }
        return null;
    }

    public boolean clearCart(){
        User userLoggedIn = getUserLoggedIn();
        if(userLoggedIn!=null){
            userLoggedIn.getCart().clear();
            userRep.save(userLoggedIn);
            return true;
        }
        return false;
    }

    public List<Orders> getOrders() {
        User userLoggedIn = getUserLoggedIn();
        if(userLoggedIn!=null){
            return userLoggedIn.getOrders();
        }
        return null;//It has to be null instead of the recommendation; Because I need a distinction between an empty list (no previous orders) and nothing at all (not logged)
    }

    public void save(User user){
        userRep.save(user);
    }
}
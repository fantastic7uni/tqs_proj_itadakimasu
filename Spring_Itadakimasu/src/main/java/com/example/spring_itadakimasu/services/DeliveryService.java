package com.example.spring_itadakimasu.services;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import org.json.JSONObject;

@Service
public class DeliveryService {

    private RestTemplate restTemplate;
    //private final String baseUrl = "http://localhost:8081";
    private final String baseUrl = "http://192.168.160.223:8081";

    public DeliveryService() {
        restTemplate = new RestTemplate();
    }

    public void postOrder(JSONObject requestJSON) {
        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<>(requestJSON.toString(), headers);
        restTemplate.postForObject(baseUrl + "/api/deliveries/assign", request, String.class);
    }
}

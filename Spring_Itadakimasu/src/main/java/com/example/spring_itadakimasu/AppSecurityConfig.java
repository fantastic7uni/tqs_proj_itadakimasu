package com.example.spring_itadakimasu;

import com.example.spring_itadakimasu.services.UserManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.servlet.PathRequest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
public class AppSecurityConfig extends WebSecurityConfigurerAdapter{

    @Autowired
    private UserManager userManager;

    @Bean
    public AuthenticationProvider authProvider(){
        var provider = new DaoAuthenticationProvider();

        provider.setUserDetailsService(userManager); //with this we can fetch data from the database
        provider.setPasswordEncoder(NoOpPasswordEncoder.getInstance()); //still need encryption

        return provider;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http    .csrf().disable()
                .authorizeRequests()
                .requestMatchers(PathRequest.toStaticResources().atCommonLocations()).permitAll()
                .and()
                .authorizeRequests().antMatchers("/img/**","/webfonts/**", "/fonts/**").permitAll()
                .and()
                .authorizeRequests().antMatchers("/api/**","/swagger-ui.html","/order/details","/meals","/","/food","/account/do_register","/account/register","/account/login").permitAll()
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage("/account/login").usernameParameter("email").permitAll()
                .and()
                .formLogin()
                .defaultSuccessUrl("/")
                .and()
                .logout().invalidateHttpSession(true)
                .clearAuthentication(true)
                .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                .logoutSuccessUrl("/").permitAll();

    }

}
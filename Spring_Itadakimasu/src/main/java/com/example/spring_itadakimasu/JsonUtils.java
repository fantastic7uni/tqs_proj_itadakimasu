package com.example.spring_itadakimasu;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.List;

public class JsonUtils {
    public static String convertObjectToJsonString(List<?> objectList) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.writeValueAsString(objectList);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
    }
}

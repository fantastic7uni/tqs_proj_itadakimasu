package com.example.spring_itadakimasu;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import com.example.spring_itadakimasu.entities.Address;
import com.example.spring_itadakimasu.entities.Meal;
import com.example.spring_itadakimasu.entities.User;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class UserPrincipal implements UserDetails{

    private final User user;

    public UserPrincipal(User user){
        super();
        this.user = user;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.singleton(new SimpleGrantedAuthority("USER"));
    }

    public String getfirstName() { return user.getFirstName(); }
    public String getEmail(){return user.getEmail();}
    public int getId(){return user.getId();}

    @Override
    public String getPassword() {
        return user.getPassword();
    }

    @Override
    public String getUsername() {
        return user.getEmail();
    }

    public List<Address> getAddresses(){
        return user.getAddresses();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true; //true for testing porpuses
    }

    @Override
    public boolean isAccountNonLocked() {
        return true; //true for testing porpuses
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true; //true for testing porpuses
    }

    @Override
    public boolean isEnabled() {
        return true; //true for testing porpuses
    }


}
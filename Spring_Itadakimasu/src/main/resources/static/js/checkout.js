function getCart () {
    $.ajax({
        type: "GET",
        url: "../api/account/cart",

        success: function (result) {
            console.log("Success: ", result);
            document.getElementById("scrollstyle-4").innerHTML=''; // empty cart
            let totalPrice = 0;

            // Process response content
            customizeButtons(result);
            $.each(result,
                function (i, cart_item) {
                    totalPrice+= cart_item.qty * cart_item.meal.price;
                    console.log("found an item!");
                    buildCartCard(cart_item);
                });
            showTotalPrice(totalPrice);
        },
        error: function () {
            console.log("error!!!");
        }
    });
}
function remFromCart(id){
    $.ajax({
        type: "DELETE",
        url: "../api/account/cart/" + id ,
        success: function (answer) {
            console.log("Success: ", answer);
            getCart();
        },
        error: function (e) {
            console.log("ERROR: ", e);
        }
    });
}
function doOrder() {
    console.log("choosen address value:"+ $("#delivery_address").val());
    if($("#delivery_address").val()== "address_id"){
        addModal("red","Error","You must choose an address first.")
        temporaryShowModal(2000);
    }
    else {
        $.ajax({ //Ajax makes GET request to the API
            type: "GET",
            url: "../api/account/order/" + $("#delivery_address").val(),

            success: function (result) {
                addModal('forestgreen', 'Success', "Order Completed!");
                temporaryShowModal(1100);
                $(".buy").prop('disabled', true);
                $(".buy").css('background-color', 'gray');
                $(".buy").css('border-color', 'black');
                getCart();
            },
            error: function (e) {
                console.log("Error:", e);
                addModal('red', 'Failure', e.responseText);
                temporaryShowModal(1700);
            }
        });
    }
}


function addModal(color,type,message){
    $("body").prepend(`<!-- Success modal-->
    <div class="modal fade" id="mymodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel" style="color: ${color}; bold;">${type}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    ${message}
                </div>
            </div>
        </div>
    </div>`);
}


function temporaryShowModal(time){
    $("#mymodal").modal('show');
    setTimeout(function(){
        $("#mymodal").modal('hide');
        $("#modal").remove();
        $('.modal-backdrop').remove();
    }, time);
}


function customizeButtons(result){
    if(result.length > 0){
        console.log("not empty");
        $(".buy").prop('disabled', false);
        $(".buy").css('background-color','green');
        $(".buy").css('border-color','green');
    }
    else{
        console.log("empty");
        $(".buy").prop('disabled', true);
        $(".buy").css('background-color','gray');
        $(".buy").css('border-color','black');
    }
}
function showTotalPrice(totalPrice){
    let card = `<div class="item-total">
                                    <div class="total-price border-0 pb-0"><span class="text-dark-white fw-600">Items subtotal:</span>
                                        <span class="text-dark-white fw-600">$${totalPrice}</span>
                                    </div>
                                    <div class="total-price border-0 pt-0 pb-0"><span class="text-light-green fw-600">Delivery fee:</span>
                                        <span class="text-light-green fw-600">Free</span>
                                    </div>
                                    <div class="total-price border-0 "><span
                                            class="text-light-black fw-700">Total:</span>
                                        <span class="text-light-black fw-700">$${totalPrice}</span>
                                    </div>
                                </div>`;
    $('#scrollstyle-4').append(card);
}

function buildCartCard( cart_item) {
    let card = `<div class="cat-product-box" id="cart_item_${cart_item.id}">
                <div class="cat-product">
                  <div class="cat-name">
                                                    <a href="#">
                                                        <p class="text-light-green fw-700"><span
                                                                class="text-dark-white">${cart_item.qty}</span>${cart_item.meal.name}</p>
                                                        <span class="text-light-white">${cart_item.meal.description}</span>
                                                    </a>
                                                </div>
                                                <div class="delete-btn">
                                                    <a href="#" class="text-dark-white"> <i
                                                            class="far fa-trash-alt"></i>
                                                    </a>
                                                </div>
                                                <div class="price"><a href="#" class="text-dark-white fw-500">
                                                    $${cart_item.meal.price * cart_item.qty}
                                                </a>
                                                </div>
                                            </div></div>`;

    $('#scrollstyle-4').prepend(card);
    $("#cart_item_"+cart_item.id).click(function(){
        remFromCart(cart_item.id);
    });
}



function getDeliveryAddresses() {
    $.ajax({
        type: "GET",
        url: "/api/account/addresses",

        success: function (result) {
            console.log("Success: ", result);
            $.each(result,
                function (i, address) {
                    let entry = `<option value="${address.addressId}">${address.address}</option>`;
                    $("#delivery_address").append(entry);
                });
        },
        error: function (e) {
            console.log(e.responseText());
            console.log("error!!!");
            console.log(e.text());
        }
    });
}
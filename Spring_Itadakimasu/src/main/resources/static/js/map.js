$( document ).ready(function() {
    var mymap = L.map('mapid').setView([39.62357047667986, -8.085167104767029], 7);

    var myIcon = L.divIcon({
        html: '<img style="width:48px" src="../../img/jet.png"/>'+
        '<span class="my-div-span" style="color:black;font-weight:bold;">Rider</span>',
        className:"rider"
    });

    var myIcon2 = L.divIcon({
        html: '<img style="width:48px" src="../../img/tracking.png"/>'+
            '<span class="my-div-span" style="color:black;font-weight:bold;">Destination</span>',
        className:"dest"
    });

    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 18,
        id: 'mapbox/streets-v11',
        tileSize: 512,
        zoomOffset: -1,
        accessToken: 'pk.eyJ1IjoiYmx1ZWRyYWdvbjY5IiwiYSI6ImNrcTg4MW5xajAwZzYyb3FzY3B0cG0xYmwifQ.FLvilzBoeTn5Jxpwi3Sjig'
    }).addTo(mymap);
    let layerGroup = L.layerGroup().addTo(mymap);
    let id = $("#id").html();
    let destLat = $("#destLat").html();
    let destLon = $("#destLon").html();
    L.marker([destLat, destLon], { icon: myIcon2, }).setZIndexOffset(-1).addTo(mymap);
    setInterval(function(){mark(id,layerGroup,myIcon);},1000);
});

function mark (id, lg, myIcon) {
    $.ajax({
        type: "GET",
        url: "http://localhost:8081/api/rider/location?rider="+id,
        success: function (result) {
            console.log("called");
            lg.clearLayers();
            L.marker([result[0],result[1]], { icon: myIcon }).addTo(lg);
        },
        error: function (e) {
            console.log(e.text);
            console.log("error!!!");
        }
    });
}




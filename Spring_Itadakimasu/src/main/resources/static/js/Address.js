
        function addAddress() {
            $.ajax({ //Ajax makes POST request to the API
                type: "POST",
                contentType: "application/json",
                url: "../../api/account/addresses",
                data: JSON.stringify(
                    {
                        country: $("#country").val(),district: $("#district").val(),address: $("#streetName").val(),zipCode: $("#postalCode").val()
                    }),
                //On Success
                success: function (result) {
                    console.log("Success: ", result);
                    window.location.href="/account/address";
                },
                error: function (e) {
                    console.log("ERROR: ", e);
                    $('#addressFields').append(
                        '<div style="margin: auto;  font-size:25px; width:80%; text-align:center" class="alert alert-danger" role="alert">\n' +
                        '  Could not add a new Address!\n' +
                        '</div>'
                    );
                }
            });
        }




function getAddresses () {
    $.ajax({
        type: "GET",
        url: "/api/account/addresses",

        success: function (result) {
            document.getElementById("addresses-box").innerHTML=''; // empty addresses list
            console.log("Success: ", result);
            $.each(result,
                function (i, address) {
                    buildAddressCard(i,address);
                });
        },
        error: function (e) {
            console.log(e.text);
            console.log("error!!!");
            console.log(e.text());
        }
    });
}

function buildAddressCard(i,address){
    let card = `<div class="w3-card-4" style="width:40%">
                        <header class="w3-container w3-light-grey">
                            <h3>Address ${i+1}  </h3>
                        </header>
                        <div class="w3-container">
                            <p><b>Address:</b> ${address.address}</p>
                            <hr>
                            <p><b>Country:</b> ${address.country} </p>
                            <p><b>District:</b> ${address.district} </p>
                            <p><b>Zip Code:</b> ${address.zipCode} </p>
                        </div>  
                        <button id="address_${address.addressId}" class="w3-button w3-block w3-red">Delete</button>
                    </div>`;

    $("#addresses-box").append(card);
    $("#address_"+address.addressId).click(function(){
        remAddress(address.addressId);
    });
}


function remAddress(address){
    console.log("inside rem address " + address);
    $.ajax({
        type: "DELETE",
        url: "../api/account/addresses/" + address ,
        success: function (answer) {
            console.log("Success: ", answer);

            getAddresses();
        },
        error: function (e) {
            console.log("ERROR: ", e);
        }
    });
}
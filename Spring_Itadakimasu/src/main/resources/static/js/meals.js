$(document).ready(
    function () {
        let dataString; //Search String
        //Function to call the API with the Search String and populate the page
        function ajaxGet() {
            $.ajax({ //Ajax makes GET request to the API
                type: "GET",
                url: "api/meals?"+dataString,
                //On Success Populate the page
                success: function (result) {
                    $.each(result,
                        function (i, meal) {
                            console.log(meal + "____" + i);
                            buildMealCard(i,meal);
                        });
                    console.log("Success: ", result);
                },
                error: function () {
                    $('#myproducts-row').append(
                        '<div style="margin: auto;  font-size:25px; width:80%; text-align:center" class="alert alert-danger" role="alert">\n' +
                        '  No meals were found!\n' +
                        '</div>'
                    );
                }
            });
        }

        //This will allow us to redirect users with other functions and reload the script as this is the first code called
        //If we are in the results page and the Search String is not empty
        if(window.location.href.includes("meals") && sessionStorage.getItem("search")!=""){
            dataString = sessionStorage.getItem("search"); //Then we want to get the Search String from the storage (which survives script reloads)
            ajaxGet(); //And call the ajaxGet() function which will call the API
            sessionStorage.setItem("search",""); //Then we clear the storage to show the Search String was consumed
        }

        //On Form submission a.k.a user makes a search
        $("form").on("submit", function (e) {

            dataString = $(this).serialize(); //Form data
            $('#myproducts-row').empty(); //Clear cards as this might not be the first search
            $.ajax({ //Ajax now has the Search String as it comes from the form data
                data: dataString,
                success: function () { // On success
                    //Check if we're not in the results page
                    if(!window.location.href.includes("meals")){ //If we are not
                        sessionStorage.setItem("search", dataString); //Save the search string because of script reload
                        window.location.href = "/meals"; //Redirect to the results page
                    }
                    ajaxGet(); //Call the API and populate the page if we are in the right page
                }
            });
            e.preventDefault();
        });
    });




function buildMealCard(i,meal){
    let card =  `<div class="col-lg-12">
<div class="restaurent-product-list">
    <div class="restaurent-product-detail">
        <div class="restaurent-product-left">
            <div class="restaurent-product-title-box">
                <div class="restaurent-product-box">
                    <div class="restaurent-product-title">
                        <h6 class="mb-2" data-toggle="modal" data-target="#restaurent-popup"><a href="javascript:void(0)" class="text-light-black fw-600">${meal.name}</a></h6>
                        <p class="text-light-white">600-700 Cal.</p>
                    </div>
                <div class="restaurent-product-label"> <span class="rectangle-tag bg-gradient-red text-custom-white">Label</span>
                <span class="rectangle-tag bg-dark text-custom-white">Combo</span>
            </div>
        </div>
        <div class="restaurent-product-rating">
            <div class="ratings"> <span class="text-yellow"><i class="fas fa-star"></i></span>
                <span class="text-yellow"><i class="fas fa-star"></i></span>
                <span class="text-yellow"><i class="fas fa-star"></i></span>
                <span class="text-yellow"><i class="fas fa-star"></i></span>
                <span class="text-yellow"><i class="fas fa-star-half-alt"></i></span>
        </div>
        <div class="rating-text">
            <p class="text-light-white fs-12 title">3845 ratings</p>
        </div>
    </div>
</div>
<div class="restaurent-product-caption-box"> <span class="text-light-white">${meal.description}</span>
</div>
<div class="restaurent-tags-price">
    <div class="restaurent-tags"> <span class="text-custom-white square-tag">
        <img src="/img/svg/004-leaf.svg" alt="tag">
        </span>
        <span class="text-custom-white square-tag">
        <img src="/img/svg/005-chef.svg" alt="tag">
        </span>
                      <span class="text-custom-white square-tag">
                      <img src="/img/svg/008-protein.svg" alt="tag">
                      </span>
                      <span class="text-custom-white square-tag">
                      <img src="/img/svg/009-lemon.svg" alt="tag">
                      </span>
                      </div> <span id="meal_${meal.id}" class="circle-tag">
                      <i class="fas fa-shopping-cart"></i>
                      </span>
                     <div class="restaurent-product-price">
                         <h6 class="text-success fw-600 no-margin">${meal.price}</h6>
                    </div>
                </div>
            </div>
            <div class="restaurent-product-img">
                <img src="/img/dish/150x151/dish-4.jpg" class="img-fluid" alt="#">
            </div>
        </div>
    </div>
</div>`;


    $('#myproducts-row').append(card);

    $("#meal_"+meal.id).click(function(){
        addtoCart(meal.id);
    });


    return card;
}


function addtoCart(id){
    $.ajax({
        type: "POST",
        url: "api/account/cart/" + id ,
        success: function (answer) {
            console.log("Success: ", answer);
            getCart();
        },
        error: function (e) {
            console.log("ERROR: ", e);
            addModal('red','Failure',e.responseText);
            temporaryShowModal(1700);
        }
    });
}

function addModal(color,type,message){
    $("body").prepend(`<!-- Success modal-->
    <div class="modal fade" id="mymodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel" style="color: ${color}; bold;">${type}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    ${message}
                </div>
            </div>
        </div>
    </div>`);
}

function temporaryShowModal(time){
    $("#mymodal").modal('show');
    setTimeout(function(){
        $("#mymodal").modal('hide');
        $("#modal").remove();
        $('.modal-backdrop').remove();
    }, time);
}
function ajaxGet() {
    $.ajax({ //Ajax makes GET request to the API
        type: "GET",
        url: "/api/account/orders",
        //On Success Populate the page
        success: function (result) {
            console.log(result);
            result.forEach(populate)
        },
        error: function () {
            $(".timeline").empty();
            $('#all').append(
                '<div style="margin: auto;  font-size:25px; width:80%; text-align:center" class="alert alert-danger" role="alert">\n' +
                '  No orders were found!\n' +
                '</div>'
            );
        }
    });
}

function populate(item, index) {
    let orderItems = "";
    item.qtyMeals.forEach(function(orderItem){
        orderItems += `<p>     Meal: ${orderItem.meal.name} | Quantity: ${orderItem.qty} </p>`;
    });
    let card = `<li>
                <a>Id: ${item.id}</a>
                <a class="float-right">${item.date}</a>
                <p>Status: ${item.orderStatus} | Quantity: ${item.qtyOrdered} | Total: $${item.totalPrice}</p>
                ${orderItems}
                <a href="/account/trackorder/${item.id}"><button class="btn btn-primary">Track Order</button></a>
                </li>`;
    $(".timeline").append(card);
}



ajaxGet();